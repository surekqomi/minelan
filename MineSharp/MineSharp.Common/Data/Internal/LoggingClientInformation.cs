namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Contains information about client logging
    /// </summary>
    public class LoggingClientInformation
    {
        /// <summary>
        /// Logging argument
        /// </summary>
        public string Argument;
        
        /// <summary>
        /// Logging type
        /// </summary>
        public string Type;
        
        /// <summary>
        /// Logging file
        /// </summary>
        public LoggingFileInformation File;
    }
}