namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Contains information about Logging File
    /// </summary>
    public class LoggingFileInformation
    {
        /// <summary>
        /// File ID
        /// </summary>
        public string Id;
        
        /// <summary>
        /// File checksum
        /// </summary>
        public string Sha1;
        
        /// <summary>
        /// File size
        /// </summary>
        public long Size;
        
        /// <summary>
        /// File URI
        /// </summary>
        public string Url;
    }
}