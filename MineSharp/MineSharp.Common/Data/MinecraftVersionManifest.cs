using System;
using System.Collections.Generic;
using MineSharp.Common.Data.Internal;

namespace MineSharp.Common.Data
{
    /// <summary>
    /// Represents Minecraft Version Manifest
    /// </summary>
    public class MinecraftVersionManifest
    {
        
        /// <summary>
        /// Asset Index for Version
        /// </summary>
        public AssetIndex AssetIndex;
        
        /// <summary>
        /// Assets for version
        /// </summary>
        public string Assets;
        
        /// <summary>
        /// Downloads for version
        /// </summary>
        public DownloadsGroupInformation Downloads;
        
        /// <summary>
        /// Version ID
        /// </summary>
        public string Id;
        
        /// <summary>
        /// Version required libraries
        /// </summary>
        public List<LibraryInformation> Libraries = new List<LibraryInformation>();
        
        /// <summary>
        /// Version logging information
        /// </summary>
        public LoggingInformation Logging;
        
        /// <summary>
        /// Main class
        /// </summary>
        public string MainClass;
        
        /// <summary>
        /// Cusotom game arguments
        /// </summary>
        public string MinecraftArguments;
        
        /// <summary>
        /// Minimum version of launcher
        /// </summary>
        public int MinimumLauncherVersion;
        
        /// <summary>
        /// Release Date
        /// </summary>
        public string ReleaseTime;
        public string Time;
        
        /// <summary>
        /// Version Type
        /// </summary>
        public string Type;

        /// <summary>
        /// Converts Time to DateTime format
        /// </summary>
        /// <returns></returns>
        public DateTime GetTime()
        {
            return DateTime.Parse(Time);
        }

        /// <summary>
        /// Converts ReleaseTime to DateTime format
        /// </summary>
        /// <returns></returns>
        public DateTime GetReleaseTime()
        {
            return DateTime.Parse(ReleaseTime);
        }
    }
}