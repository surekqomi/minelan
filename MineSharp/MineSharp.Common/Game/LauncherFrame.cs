using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using MineSharp.Common.Data;
using MineSharp.Common.Network;
using Newtonsoft.Json;

namespace MineSharp.Common.Game
{
    /// <summary>
    /// LauncherFrame is main class related to downloading and launching game.
    /// Here you also set all parameters.
    /// </summary>
    public class LauncherFrame
    {
        
        /// <summary>
        /// Player's name
        /// </summary>
        public string Username = "Player";
        
        /// <summary>
        /// Access token, commonly used with Mojang Auth Servers, can be default one for
        /// non-premium launchers
        /// </summary>
        public string AccessToken = "aeef7bc935f9420eb6314dea7ad7e1e5";
        
        /// <summary>
        /// User ID, 24-hex (0-F) string
        /// </summary>
        public string Uuid = "N/A";
        
        /// <summary>
        /// Type of user - mojang or legacy
        /// </summary>
        public string UserType = "mojang";

        /// <summary>
        /// Max RAM for game, defined in MiB
        /// </summary>
        public int MaxRamMb;
        
        /// <summary>
        /// Min RAM for game, defined in MiB
        /// </summary>
        public int MinRamMb;

        /// <summary>
        /// Game window width
        /// </summary>
        public int Width;
        
        /// <summary>
        /// Game window height
        /// </summary>
        public int Height;
        
        /// <summary>
        /// Game version
        /// </summary>
        public string Version;
        
        /// <summary>
        /// Set this to true if you want to load Minecraft with Forge
        /// </summary>
        public bool UseForge = false;


        /// <summary>
        /// Creates command and launches game in background
        /// </summary>
        /// <param name="gameDir">Game Directory</param>
        /// <param name="nativesPath">Natives Directory</param>
        /// <param name="assetDir">Assets Directory</param>
        /// <param name="libDir">Libraries Directory</param>
        public void RunGameBackground(string gameDir, string nativesPath, string assetDir, string libDir)
        {
            //Create new Process and assign parameters
            var p = new Process();
            p.StartInfo = new ProcessStartInfo("java");
            p.StartInfo.CreateNoWindow = true;
            
            //Create command and assign it as argument
            p.StartInfo.Arguments = CreateCommand(gameDir, nativesPath, assetDir, libDir);
            
            //Start game
            p.Start();
        }

        /// <summary>
        /// Creates command and launches game
        /// </summary>
        /// <param name="gameDir">Game Directory</param>
        /// <param name="nativesPath">Natives Directory</param>
        /// <param name="assetDir">Assets Directory</param>
        /// <param name="libDir">Libraries directory</param>
        public void RunGame(string gameDir, string nativesPath, string assetDir, string libDir)
        {
            Process.Start("java", CreateCommand(gameDir, nativesPath, assetDir, libDir));
        }

        /// <summary>
        /// Downloads Game
        /// </summary>
        /// <param name="gameDir">Game Directory</param>
        /// <param name="nativesPath">Natives Location</param>
        /// <param name="assetDir">Assets Directory</param>
        /// <param name="libDir">Library Directory</param>
        public void DownloadMinecraft(string gameDir, string nativesPath, string assetDir, string libDir)
        {
            var man = AccquireManifest(gameDir);
            man.DownloadAssets(assetDir);
            man.DownloadLibraries(libDir, nativesPath);
            man.DownloadMinecraft(gameDir);
        }

        /// <summary>
        /// Downloads Game Manifest for Version specified in this Frame
        /// </summary>
        /// <param name="gameDir">Directory to download into</param>
        /// <returns>Manifest Object or null if Version is wrong</returns>
        public MinecraftVersionManifest AccquireManifest(string gameDir)
        {
            //Downloads Minecraft
            var vm = DownloadManager.DownloadVersionsManifest().DownloadVersion(Version);
            if (vm == null)
            {
                try
                {
                    //Deserialize downloaded manifest
                    var dat = File.ReadAllText(gameDir + "/versions/" + Version + "/" + Version + ".json");
                    vm = JsonConvert.DeserializeObject<MinecraftVersionManifest>(dat);
                }
                catch(Exception ex)
                {
                    //It does not exist or is not JSON file...
                }
            }

            return vm;
        }



        /// <summary>
        /// Creates Minecraft Launch Command using specified parameters
        /// </summary>
        /// <param name="gameDir">Game main directory</param>
        /// <param name="nativesPath">Location of native libraries</param>
        /// <param name="assetDir">Location where game assets will be downloaded</param>
        /// <param name="libDir">Location of libraries</param>
        /// <param name="customLibs">List of custom libraries to be appended to ClassPath.
        /// Remember to start with ; and not include ; in the end...</param>
        /// <returns>String that can be used with ProcessStart("java") to launch Minecraft</returns>
        public string CreateCommand(string gameDir, string nativesPath, string assetDir, string libDir,
            string customLibs = "")
        {
            //Add forward slashes at the end of directory paths, to prevent bugs while appending files
            if (!libDir.EndsWith("/")) libDir += "/";
            if (!gameDir.EndsWith("/")) gameDir += "/";
            if (!nativesPath.EndsWith("/")) nativesPath += "/";
            if (!assetDir.EndsWith("/")) assetDir += "/";

            
            
            //Download manifest
            var vm = AccquireManifest(gameDir);
            if (vm != null)
            {
                //Start creating command using StringBuilder
                var cmd = new StringBuilder();
                
                //Append RAM values
                if(MinRamMb > 0)
                    cmd.Append("-Xms" + MinRamMb + "M ");
                if (MaxRamMb > 0)
                    cmd.Append("-Xmx" + MaxRamMb + "M ");

                //Append Mojang libraries and drivers to improve performance
                cmd.Append("-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump ");
                
                //Choose natives directory
                cmd.Append("-Djava.library.path=" + nativesPath + " ");
                
                //Build and add ClassPath
                cmd.Append("-cp ");
                var n = false;
                
                //Add all Libraries to ClassPath
                foreach (var v in vm.Libraries)
                {
                    //If library does not exists, just ignore
                    if (v == null) continue;
                    
                    //Append semicolon to split Libraries in ClassPath
                    if (n)
                        cmd.Append(";");

                    //Select Artifact path
                    var dn = libDir + v.Downloads.GetArtifact().Path;
                    
                    //Append library to ClassPath
                    cmd.Append(dn);

                    n = true;
                }
                
                //Append ClassPath to launch command
                cmd.Append(customLibs);

                //Append game version artifact to command
                cmd.Append(";" + gameDir + "versions/" + vm.Id + "/" + vm.Id + ".jar ");
                
                //If using forge, select Forge launcher
                if (UseForge)
                    cmd.Append("net.minecraft.launchwrapper.Launch ");
                else
                    cmd.Append("net.minecraft.client.main.Main ");
                
                //Select Game Window size
                if (Width > 0)
                    cmd.Append("--width " + Width + " ");
                if (Height > 0)
                    cmd.Append("--height " + Height + " ");

                //Select User/Game data including access data, username, version, assets version etc.
                cmd.Append("--username " + Username + " ");
                cmd.Append("--version " + Version + " ");
                cmd.Append("--gameDir " + gameDir + " ");
                cmd.Append("--assetsDir " + assetDir + " ");
                cmd.Append("--assetIndex " + vm.Assets + " ");
                cmd.Append("--uuid " + Uuid + " ");
                cmd.Append("--accessToken " + AccessToken + " ");
                cmd.Append("--userType " + UserType + " ");
                
                //If using forge add Tweaks and set Version type
                //otherwise set Version type to vanilla
                if (UseForge)
                {
                    cmd.Append("--tweakClass net.minecraftforge.fml.common.launcher.FMLTweaker ");
                    cmd.Append("--versionType Forge");
                }
                else
                {
                    cmd.Append("--versionType Vanilla");
                }

                //Return command
                return cmd.ToString();
            }
            else
            {
                //Command cannot be created
                throw new Exception("Command cannot be created, manifest not downloaded.");
            }
            
        }
        
    }
}